
# Studentflex Dev Puzzle

<br></br>
Awesome that you have made it this far! 🤖

[You can check out the puzzle here](https://gitlab.com/public-leon/studentflex_code_puzzle/-/blob/master/puzzle_description.md)

What could be the solution?  
How did I develop it?   
Did I modify the schedule coordinates every time by hand?  

### How Do You Go From Fun Puzzle to Interactive Prototype?

<img src="https://giant.gfycat.com/IncompleteSerpentineBlackpanther.webm" width="100%" height="700px">

<br></br>

## Spoiler alert... 

![spoiler alert](https://i.pinimg.com/originals/b9/dc/bc/b9dcbc832d34d1e0eb53df04a850c1cf.jpg)

<br></br>

## The answer is

```
SIDE JOB AT STUDENTFLEX
```  

##### Basic
[C# basic solution](https://gitlab.com/public-leon/studentflex_code_puzzle/-/blob/master/simple%20solutions/c%23_simple_solution.cs)

##### More Interesting
[JS solution 2](https://gitlab.com/public-leon/studentflex_code_puzzle/-/blob/master/other%20solutions/js_other_solution.js)  
[C# solution 2](https://gitlab.com/public-leon/studentflex_code_puzzle/-/blob/master/other%20solutions/c%23_other_solution.cs)

![diagram](https://i.ibb.co/ZJFWPMr/c-solution-2-diagram.jpg)

<br/><br/>

## API endpoint

<img src="https://memegenerator.net/img/instances/400x/72654650/please-talk-to-me.jpg" height="300">

<br/><br/>

[Puzzle Development](https://gitlab.com/public-leon/studentflex_code_puzzle/-/blob/master/js_puzzle_development.js)  
[Backend api](https://gitlab.com/public-leon/studentflex_code_puzzle/-/tree/master/app/backend-api)

## Interactive APP
  
[Backend api](https://gitlab.com/public-leon/studentflex_code_puzzle/-/tree/master/app/backend-api)  
[Frontend](https://gitlab.com/public-leon/studentflex_code_puzzle/-/tree/master/app/frontend)

<img src="https://giant.gfycat.com/IncompleteSerpentineBlackpanther.webm" width="100%" height="700px">

![app_diagram](https://i.ibb.co/872mtmt/app-diagram.jpg)

<br/><br/>

## So Now What?

[Side job at Studentflex](https://www1.studentflex.nl/index.php/page/applicants/bb/1/command/startlogin/cms_categorie/8491)

leonvanderstel.work@gmail.com

### Search Terms

##### Backend API
- [Node js](https://nodejs.org/en/docs/guides/getting-started-guide/)
- [Express js](https://expressjs.com/)
- [Nodemon](https://www.digitalocean.com/community/tutorials/workflow-nodemon)
- [NPM - node package manager](https://www.sitepoint.com/npm-guide/)

#### Frontend
- [State management](https://openexpoeurope.com/the-10-best-tools-for-state-management-in-front-end/)
- [MobX](https://mobx.js.org/getting-started)
- [jquery](https://jquery.com/)
- [Front end frameworks](https://www.simform.com/best-frontend-frameworks/#:~:text=Top%203%20Frontend%20Frameworks%20of,three%20frontend%20frameworks%20of%202020.)
- [Axios](https://stackabuse.com/making-asynchronous-http-requests-in-javascript-with-axios/)
- [Webpack Dev Server](https://bendyworks.com/blog/getting-started-with-webpack-dev-server)

#### Other
- [C# Linq guide](https://www.tutorialspoint.com/linq/linq_quick_guide.htm)
- [C# Linq joining lists](https://stackoverflow.com/questions/6253656/how-do-i-join-two-lists-using-linq-or-lambda-expressions)
- [JS reduce function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce)
- [JS map function](https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Global_Objects/Array/map)
- [JS reverse function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse)
- [JS arrow unctions](https://www.telerik.com/blogs/the-guide-to-arrow-functions-in-es6)
- [JS find function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find)
- [Babel](https://medium.com/@abs4real16.ma/writing-es6-in-nodejs-using-babel-10731b8032fc)
- [Lodash js lib](https://medium.com/@hakhoipham/the-beginners-guide-to-lodash-58f98599da54)
- [Lodash Debounce function](https://masteringjs.io/tutorials/lodash/debounce)
- [JS decorators](https://www.sitepoint.com/javascript-decorators-what-they-are/)

© Leon van der Stel
