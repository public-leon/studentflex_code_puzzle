#include <iostream>

char
getChar(int row, int col)
{
  const char ciphixOfficeGird[5][6] =
  {
    {'A', 'B', 'C', 'D', 'E', 'F'},
    {'G', 'H', 'I', 'J', 'K', 'L'},
    {'M', 'N', 'O', 'P', 'Q', 'R'},
    {'S', 'T', 'U', 'V', 'W', 'X'},
    {'Y', 'Z'}
  };
  if(!(row || col)) return ' ';
  --row;
  --col;
  return ciphixOfficeGird[row][col];
}

int
main()
{
  int buffer = 0;
  const int workSchedule[5][5] =
  {
    {41, 23, 14, 15, 00},
    {24, 33, 12, 00, 11},
    {42, 00, 41, 42, 43},
    {14, 15, 32, 42, 16},
    {26, 15, 46, 00, 00}
  };

  for(int rows = 0; rows < 5; ++rows)for(int cols = 0; cols < 5; ++cols)
  {
    int num = workSchedule[rows][cols];
    int rowNum = num / 10;
    int colNum = num % 10;
    char current = getChar(rowNum, colNum);
    if(current == ' ')
    {
      ++buffer;
    }
    else
    {
      for(int spc = 0; spc < buffer; ++spc) std::cout << " ";
      buffer = 0;
      std::cout << current;
    }
  }
}
