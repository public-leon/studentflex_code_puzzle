package main

import (
	"bytes"
	"fmt"
	"sync"
)

//    1st room       ->        6th
var ciphixBuildingGrid = [][]string{
	{"A", "B", "C", "D", "E", "F"}, // 1st floor
	{"G", "H", "I", "J", "K", "L"},
	{"M", "N", "O", "P", "Q", "R"}, // ...
	{"S", "T", "U", "V", "W", "X"},
	{"Y", "Z"}, // 5th
}

// Multidimensional array that represents the student's work schedule
// IMPORTANT! Remember that a [0,0] must be converted to a white space

//   mo        tu        wed       thur      fri
var workSchedule = [][]int{
	{4, 1}, {2, 3}, {1, 4}, {1, 5}, {0, 0}, // week 1
	{2, 4}, {3, 3}, {1, 2}, {0, 0}, {1, 1}, // week 2
	{4, 2}, {0, 0}, {4, 1}, {4, 2}, {4, 3}, // week 3
	{1, 4}, {1, 5}, {3, 2}, {4, 2}, {1, 6}, // week 4
	{2, 6}, {1, 5}, {4, 6}, {0, 0}, {0, 0}, // week 5
}

const (
	space          = " "
	defaultInBatch = 5
)

func main() {
	fmt.Println(solutionWithGoRoutines())
}

type PuzzleSolver struct {
	wg          *sync.WaitGroup
	PuzzleSlice []string
}

func NewPuzzleSolver(wg *sync.WaitGroup) *PuzzleSolver {
	totalBatches := len(workSchedule) / defaultInBatch
	return &PuzzleSolver{wg: wg, PuzzleSlice: make([]string, totalBatches)}
}

func (p *PuzzleSolver) runBatch(index int, job int) {
	defer p.wg.Done()

	var b bytes.Buffer
	part := workSchedule[index : index+defaultInBatch]
	for i := range part {
		r := part[i]
		if r[0] == 0 {
			b.WriteString(space)

			continue
		}

		b.WriteString(ciphixBuildingGrid[r[0]-1][r[1]-1])
	}
	p.PuzzleSlice[job] = b.String()
}

func (p *PuzzleSolver) CrackThatPuzzleG() string {
	var b bytes.Buffer
	for i := range p.PuzzleSlice {
		b.WriteString(p.PuzzleSlice[i])
	}

	return b.String()
}

func solutionWithGoRoutines() string {
	s := NewPuzzleSolver(&sync.WaitGroup{})

	for i, j := 0, 0; i < len(workSchedule); i, j = i+defaultInBatch, j+1 {
		s.wg.Add(1)
		go s.runBatch(i, j)
	}
	s.wg.Wait()

	return s.CrackThatPuzzleG()
}
