package main

import (
	"bytes"
	"fmt"
)

//    1st room       ->        6th
var ciphixBuildingGrid = [][]string{
	{"A", "B", "C", "D", "E", "F"}, // 1st floor
	{"G", "H", "I", "J", "K", "L"},
	{"M", "N", "O", "P", "Q", "R"}, // ...
	{"S", "T", "U", "V", "W", "X"},
	{"Y", "Z"}, // 5th
}

// Multidimensional array that represents the student's work schedule
// IMPORTANT! Remember that a [0,0] must be converted to a white space

//   mo        tu        wed       thur      fri
var workSchedule = [][]int{
	{4, 1}, {2, 3}, {1, 4}, {1, 5}, {0, 0}, // week 1
	{2, 4}, {3, 3}, {1, 2}, {0, 0}, {1, 1}, // week 2
	{4, 2}, {0, 0}, {4, 1}, {4, 2}, {4, 3}, // week 3
	{1, 4}, {1, 5}, {3, 2}, {4, 2}, {1, 6}, // week 4
	{2, 6}, {1, 5}, {4, 6}, {0, 0}, {0, 0}, // week 5
}

const space = " "

func main() {
	fmt.Println(solutionSimple())
}

func solutionSimple() string {
	var b bytes.Buffer
	for _, s := range workSchedule {
		if s[0] == 0 {
			b.WriteString(space)

			continue
		}
		b.WriteString(ciphixBuildingGrid[s[0]-1][s[1]-1])
	}

	return b.String()
}
