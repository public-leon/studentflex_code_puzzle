﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ciphix
{
   class Room
   {
      public override bool Equals(Object obj)
      {
         return ((FloorNr == ((Room)obj).FloorNr) && (DoorNr == ((Room)obj).DoorNr));
      }

      public override int GetHashCode()
      {
         return FloorNr * 6 + DoorNr;
      }

      public int FloorNr { get; set; }
      public int DoorNr { get; set; }
   }

   class Program
   {
      static Dictionary<Room, char> ciphixOfficeGrid = new Dictionary<Room, char>();
      static List<Room> workSchedule = new List<Room>();

      static int[,] workScheduleItems = new int[,]
      {
         { 4, 1 }, { 2, 3 }, { 1, 4 }, { 1, 5 }, { 0, 0 }, // week 1
         { 2, 4 }, { 3, 3 }, { 1, 2 }, { 0, 0 }, { 1, 1 }, // week 2
         { 4, 2 }, { 0, 0 }, { 4, 1 }, { 4, 2 }, { 4, 3 }, // week 3
         { 1, 4 }, { 1, 5 }, { 3, 2 }, { 4, 2 }, { 1, 6 }, // week 4
         { 2, 6 }, { 1, 5 }, { 4, 6 }, { 0, 0 }, { 0, 0 }  // week 5
      };

      static void Main(string[] args)
      {
         for (int floorNr = 1; floorNr <= 4; floorNr++)
         {
            for (int doorNr = 1; doorNr <= 6; doorNr++)
            {
               ciphixOfficeGrid.Add(new Room() { FloorNr = floorNr, DoorNr = doorNr }, (char)('A' + ((floorNr - 1) * 6) + doorNr - 1));
            }
         }

         ciphixOfficeGrid.Add(new Room() { FloorNr = 5, DoorNr = 1 }, 'Y');
         ciphixOfficeGrid.Add(new Room() { FloorNr = 5, DoorNr = 2 }, 'Z');
         ciphixOfficeGrid.Add(new Room() { FloorNr = 0, DoorNr = 0 }, ' ');

         for (int dayIdx = 0; dayIdx < workScheduleItems.GetUpperBound(0); dayIdx++)
         {
            workSchedule.Add(new Room() { FloorNr = workScheduleItems[dayIdx, 0], DoorNr = workScheduleItems[dayIdx, 1] });
         }

         // #### Here we go

         var query = from workItem in workSchedule
                     join room in ciphixOfficeGrid on workItem equals room.Key
                     select new { room.Value };

         // #### And the result is

         string text = "";
         foreach (var letter in query)
         {
            text  += letter.Value;
         }
         text.Trim();

         Console.WriteLine(text);
      }
   }
}
