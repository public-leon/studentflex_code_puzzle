
import express from 'express'
import cors from 'cors';
import bodyParser from 'body-parser';
import {createPuzzleVariables} from './helpers.js';

const app = express();
app.use(bodyParser.json());
app.use(cors());

const PORT = 3000;

app.get('/puzzleVariables', function (req, res) {
    let {buildingChars, solutionSentence, storyLength} = req.query;

    // sanitize data before further processing
    buildingChars = [...new Set([...buildingChars.toUpperCase()])].join('');     // remove duplicates from buildingGrid string
    solutionSentence = solutionSentence.toUpperCase();

  const puzzleVariables = createPuzzleVariables(buildingChars, solutionSentence, storyLength);
  res.json(puzzleVariables)
});

app.listen(PORT, () => {
    console.log(`Studentflex Dev Puzzle API listening at http://localhost:${PORT}`)
});
