
export const createPuzzleVariables = (buildingChars, solutionSentence, storyLength) => {
    const buildingGrid = buildingChars.split("").reduce((acc, currentVal, index) => {
        if(index % storyLength === 0){
            acc.push([currentVal]);
        }else{
            acc[acc.length-1].push(currentVal);
        }
        return acc;
    }, []);
    
    const scheduleCoos = solutionSentence.split("").map((letter) => {
        if(letter === " "){
            return [0,0];
        }
    
        return buildingGrid.reduce( (acc, store, i) => {
            const found = store.indexOf(letter.toUpperCase());
            if(found !== -1) acc = [i+1, found+1];
            return acc;
        }, undefined);
    });

    return {
        buildingGrid,
        scheduleCoos
    }
};
