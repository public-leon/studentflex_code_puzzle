import css from "./styles.css";
import {autorun, toJS} from "mobx"
import './ui';
import { updateUI, reconstructBuilding, reconstructSchedule } from "./ui";
import { appState } from "./state";
import axios from 'axios';

export async function getPuzzleVarsFromBackend(){
  try{
    const url = `http://localhost:3000/puzzleVariables?buildingChars=${appState.buildingChars}&solutionSentence=${appState.sentence}&storyLength=${appState.storyLength}`;
    const response = await axios.get(url);
    const {data} = response;
    const {buildingGrid, scheduleCoos} = data;

    appState.buildingGrid = buildingGrid;
    appState.scheduleCoos = scheduleCoos;

  } catch (e) {
    console.warn('Could not make request');
    console.warn(e);
  }
}

autorun(() => {
  updateUI(appState.buildingChars, appState.sentence, appState.storyLength);
});

autorun(() => {
  reconstructBuilding(appState.buildingGrid);
  reconstructSchedule(toJS(appState.scheduleCoos));
});


getPuzzleVarsFromBackend();
