import {makeAutoObservable, toJS} from "mobx";

class AppState {
    constructor(name) {
        this.buildingChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        this.sentence = "side job at Studentflex";
        this.storyLength = 6;
        this.buildingMinimumHeight = 4;
        this.windowMarginLeftSeparator = 1.25;
        this.windowTopSeparator = 1.6;
        this.buildingGrid = [];
        this.scheduleCoos = [];
        makeAutoObservable(this)
    }

    scheduleCoosContains(floor, room) {
        return !!toJS(this.scheduleCoos).find( (sc) => {
            if(!sc) return false;
            return sc[0] === floor+1 && sc[1] === room+1
        });
    }

    buildingGridGetChar(floor, room) {
        const rawBuildingGrid = toJS(this.buildingGrid);
        if(floor === 0 && room === 0) return "SPACE";
        if(!rawBuildingGrid[floor-1]) return "MISSING";
        if(!rawBuildingGrid[floor-1][room-1]) return "MISSING";
        return toJS(this.buildingGrid)[floor-1][room-1];
    }
}

const appState = new AppState();

export {
    appState
}
