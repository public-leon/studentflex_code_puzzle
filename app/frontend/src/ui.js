import $ from "jquery";
import {appState} from "./state"
import * as _ from 'lodash';
import {getPuzzleVarsFromBackend} from "./main";

let $inputBuildingChars;
let $inputSentence;
let $inputStoryLength;
let $property;
let $roomsContainer;
let $building;
let $scheduleContainer;

function initUI() {
    $inputBuildingChars = $('#input_buildingChars');
    $inputSentence = $('#input_sentence');
    $inputStoryLength = $('#input_story_length');
    $property = $('.property');
    $building = $('.building');
    $scheduleContainer = $('#schedule-container');
    $roomsContainer = $('#rooms-container');


    const stateDebounce = _.debounce(function($element, appStatePropName){
        appState[appStatePropName] = $element.val();
        getPuzzleVarsFromBackend();
    }, 500);

    const inputListener = ($element, appStatePropName) => {
        $element.on('input', stateDebounce.bind(this, $element, appStatePropName));
    };

    inputListener($inputBuildingChars, 'buildingChars');
    inputListener($inputSentence, 'sentence');
    inputListener($inputStoryLength, 'storyLength');

    setTimeout(function() {
        $property.toggleClass('build');
    }, 500);
}

export const updateUI = (buildingChars, sentence, storyLength) => {
    $inputBuildingChars.val(buildingChars);
    $inputSentence.val(sentence);
    $inputStoryLength.val(storyLength);
};

export const reconstructBuilding = (buildingGrid) => {
    const buildingWidth = appState.storyLength * 1.25 + appState.windowMarginLeftSeparator/2;
    const buildingHeight = appState.buildingMinimumHeight + buildingGrid.length * 1.75  ;

    $roomsContainer.empty();
    $building.css({
        'width': `${buildingWidth}em`,
        'height': `${buildingHeight}em`
    });

    // clone array
    const buildingGridReversed = [...buildingGrid];
    buildingGridReversed.reverse();

    let currentTop = -appState.windowTopSeparator/2;

    for (const [floorIndex, floor] of buildingGridReversed.entries()){

        currentTop += appState.windowTopSeparator;
        let currentMarginLeft = -appState.windowMarginLeftSeparator/2;

        for (const [roomIndex,roomChar] of floor.entries()){

            let $newWindow;
            const floorIndexOriginal = (buildingGrid.length-1) - floorIndex;

            const windowClass = `floor${floorIndex}`;
            if(appState.scheduleCoosContains(floorIndexOriginal, roomIndex)){
                $newWindow = $(`<div class="${'window ' +windowClass}">${roomChar}</div>`);
            }else{
                $newWindow = $(`<div class="${'window normal '+ windowClass}"></div>`);
            }

            currentMarginLeft += appState.windowMarginLeftSeparator;

            $newWindow.css({
                'margin-left': `${currentMarginLeft}em`,
                'top': `${currentTop}em`
            });

            $roomsContainer.append($newWindow)
        }
    }

};

export const reconstructSchedule = (scheduleCoos) => {
    $scheduleContainer.empty();

    for(const scheduleItem of scheduleCoos){

            let classes = '';
            let char = '';
            let floorNr = scheduleItem ? scheduleItem[0] :  '_';
            let roomNr = scheduleItem ? scheduleItem[1] : '_';

            if(scheduleItem){
                char = appState.buildingGridGetChar(scheduleItem[0], scheduleItem[1]);
            }else{
                char = 'MISSING';
                classes = 'missing';
            }

            const newScheduleItem = `<span class="${classes}">Floor ${floorNr} Room ${roomNr} == <strong>${char}</strong></span>`;
            $scheduleContainer.append(newScheduleItem)
    }
};

initUI();
