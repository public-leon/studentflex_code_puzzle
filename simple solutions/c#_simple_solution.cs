﻿using System;

namespace Ciphix
{
   class Program
   {
      static int[,] workSchedule = new int[,]
      {
         { 4, 1 }, { 2, 3 }, { 1, 4 }, { 1, 5 }, { 0, 0 }, // week 1
         { 2, 4 }, { 3, 3 }, { 1, 2 }, { 0, 0 }, { 1, 1 }, // week 2
         { 4, 2 }, { 0, 0 }, { 4, 1 }, { 4, 2 }, { 4, 3 }, // week 3
         { 1, 4 }, { 1, 5 }, { 3, 2 }, { 4, 2 }, { 1, 6 }, // week 4
         { 2, 6 }, { 1, 5 }, { 4, 6 }, { 0, 0 }, { 0, 0 }  // week 5
      };

      static char[,] ciphixOfficeGrid = new char[,]
      {
         {'A', 'B', 'C', 'D', 'E', 'F' }, // 1st floor
         {'G', 'H', 'I', 'J', 'K', 'L' },
         {'M', 'N', 'O', 'P', 'Q', 'R' }, // ...
         {'S', 'T', 'U', 'V', 'W', 'X' },
         {'Y', 'Z', '.', '.', '.', '.' } // 5th
      };

      static void Main(string[] args)
      {
         string sentence = "";

         for (int dayIdx = 0; dayIdx < workSchedule.GetUpperBound(0); dayIdx++)
         {
            int floor = workSchedule[dayIdx, 0];
            int room = workSchedule[dayIdx, 1];

            if ((floor == 0) || (room == 0))
            {
               sentence += ' ';
            }
            else
            {
               sentence += ciphixOfficeGrid[floor - 1, room - 1];
            }
         }

         sentence.Trim();

         Console.WriteLine(sentence);
      }
   }
}
