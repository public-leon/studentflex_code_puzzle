
### The Secret Code Challenge


Objective
Be creative and write some code, with your language of choice, to find the hidden message. Did you find it? Register at https://www.studentflex.nl/tech-game/ to get the explanation and to improve yourself.

Puzzle
After contacting Studentflex for a job alongside your study you have found your number one match at Ciphix, a young and ambitious IT company. Filled with joy, you have already received your work schedule of the following year.
You will work quite a lot! Every day when you are scheduled you will work in a room within the Ciphix office. Combine your work schedule with the Ciphix office in such a way that a sentence is being revealed. The variables are given on the next page..
Every work day is linked to a room of the office which represents a letter and must be decoded with ascending dates. The days when you won’t work must be converted to a white space. For example, item workSchedule[0] represents a work day on the fourth floor and the first room and item workSchedule[4] is a day that you don’t work.

### Code
*This is written in javascript, you can use any other language you like!*
```

// Multidimensional array that represents the student's work schedule
// IMPORTANT! Remember that a [0,0] must be converted to a white space

//	     mo    	   tu    	 wed       thur      fri
const workSchedule = [
	    [ 4, 1 ], [ 2, 3 ], [ 1, 4 ], [ 1, 5 ], [ 0, 0 ],    // week 1
	    [ 2, 4 ], [ 3, 3 ],	[ 1, 2 ], [ 0, 0 ], [ 1, 1 ],    // week 2
	    [ 4, 2 ], [ 0, 0 ],	[ 4, 1 ], [ 4, 2 ], [ 4, 3 ],    // week 3
	    [ 1, 4 ], [ 1, 5 ],	[ 3, 2 ], [ 4, 2 ], [ 1, 6 ],    // week 4
	    [ 2, 6 ], [ 1, 5 ],	[ 4, 6 ], [ 0, 0 ], [ 0, 0 ]	 // week 5
];


// Multidimensional array that represents the Ciphix office


//    1st room   	->    	6th
const ciphixOfficeGrid = [
   [ 'A', 'B', 'C', 'D', 'E', 'F' ],   // 1st floor
   [ 'G', 'H', 'I', 'J', 'K', 'L' ],
   [ 'M', 'N', 'O', 'P', 'Q', 'R' ],   // ...
   [ 'S', 'T', 'U', 'V', 'W', 'X' ],
   [ 'Y', 'Z' ]                    	   // 5th
];
 
// BONUS: Leading and trailing white spaces aren't allowed in the
// final sentence

```

![story](https://upload.wikimedia.org/wikipedia/commons/8/83/Etage1.png)

### Example
```
Item workSchedule[0] must give the letter 'S' from the ciphixOfficeGrid 
```

Now it’s your turn to decode the entire work schedule and reveal the whole sentence with some  delicious code!

Submission
- Use the ciphixOfficeGrid and workSchedule variables (page 2) in your code as starting
point and adapt where necessary to your own language.
- Every language is allowed (the variables on page 2 are written in javascript)
- Register at https://www.studentflex.nl/tech-game/ to get the explanation and possible solutions
 
 
#### Success! 
 
Still interested in a side job, but don’t have time to solve this coding puzzle? You are always welcome to reach out to us through https://www.studentflex.nl
